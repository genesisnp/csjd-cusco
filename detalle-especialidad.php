<?php
    include 'src/includes/header.php'
?>
    <main id="my-scrollbar" data-scrollbar>
        <section id="parallax" class="sct-banner scroll">
            <div class="degrade-int"></div>
            <img class="img-banner" src="assets/images/banner/serv2.jpg" alt="">
            <div class="content-title-banner container">
                <h1 class="titleBanner text-uppercase">SERVICIOS Y ESPECIALIDADES</h1>
            </div>
        </section>
        <section class="wrapper-info-theClinic pb5 pt3 bg-white">
            <div class="container">
                <?php
                    include 'src/includes/filtro.php'
                ?>
            </div>

            <div class="container pt4em">
                <div class="row">
                    <div class="col-xs-12 col-md-8 wow fadeInRight" data-wow-delay="0.25s">
                        <div class="pr3">
                            <div class="ics"><span class="icon-s6"></span></div>
                            <p class="sertit">UNIDAD DE CUIDADOS<br>INTENSIVOS (UCI)</p>
                            <p class="text-internas text-justify">Contamos con un servicio de Unidad de Cuidados acorde con la actual exigencia tecnológica siendo el primer centro de salud en el Perú en instalar superficies de cobre antimicrobiano como medida adicional en la prevención de infecciones.</p>
                            <p class="text-internas text-justify">Esta unidad permite a la clínica la resolución de casos críticos como politraumatismo, shock séptico, otros tipos de shock, infarto agudo, miocardio, arritmias cardiacas potencialmente graves post operatorio de cirugía compleja, entre otros. </p>
                            <button class="btn-primary btn btn-inSesion blanco">SEPARA TU CITA</button>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-4 citli wow fadeInUp" data-wow-delay="0.75s">
                        <div class="content-quotes">
                            <i class="icon-citas"></i>
                            <h3 class="titlesBig">RESERVAR CITA</h3>
                            <p class="p-internas text-center revcit">Ahora ya puede sacar tus citas y pagarlo en<br>línea sin necesidad de ir a la clínica.</p>
                            <form action="#" class="form form-reservarCita flex jusContentCenter flex-column" method="post"
                                id="form-quotes">
                                <div class="form__wrapper mb0-5">
                                    <input type="text" class="form__input" id="document" name="document">
                                    <label class="form__label text-center">
                                        <span class="form__label-content">Nro.de Documento</span>
                                    </label>
                                </div>
                                <div class="form__wrapper mb1-5">
                                    <input type="text" class="form__input" id="lastname" name="lastname">
                                    <label class="form__label text-center">
                                        <span class="form__label-content">Contraseña</span>
                                    </label>
                                </div>
                            </form>
                            <button class="btn-primary btn btn-inSesion">Iniciar Sesión</button>
                            <a href="#" class="internasBold rg-Cntr reg">Regístrate</a>
                            <a href="#" class="p-internas rg-Cntr cambCont sinb small pb1">¿Olvidaste tu contraseña?</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
            include 'src/includes/footer.php'
        ?>
    </main>

    <script>
        $('.row.borbg .op5s').mouseover(function (e) {     
            //get selected href
            var data2 = $(this).attr('data-target');    
            
            //set all nav tabs to inactive
            $('.row.borbg ').removeClass('active');
            
            //get all nav tabs matching the href and set to active
            $('.row.borbg  .op5s[data-target="'+data2+'"]').addClass('active');

            //active tab
            $('.tab-img').removeClass('active');
            $('.tab-img'+data2).addClass('active');

        });
        $('.row.borbg .op5s').mouseout(function (e) {     
            //get selected href
            var data2 = $(this).attr('data-target');    
            
            //set all nav tabs to inactive
            $('.row.borbg ').removeClass('active');
            
            //get all nav tabs matching the href and set to active
            $('.row.borbg  .op5s[data-target="'+data2+'"]').removeClass('active');

            //active tab
            $('.tab-img').removeClass('active');
            $('.tab-img'+data2).removeClass('active');

        })
    </script>
    <?php
            include 'src/includes/cierre.php'
        ?>