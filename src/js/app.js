window.Popper = require('popper.js').default

try {
  window.$ = window.jQuery = require('jquery')
  require('bootstrap')
} 
catch (e) {}

require ('./header')
require ('owl.carousel')
require ('./config-slider')
require ('./form')
require ('./navbar-int')
require ('./select')
require ('./banner')