$(function() {
    //parallax effect for banner
    (function() {
      var posY;
      var image = document.getElementById("parallax");
      function paralax() {
        posY = window.pageYOffset;
        image.style.top = posY * .7 + "px";
      }
      window.addEventListener("scroll", paralax);
    })();
  });
