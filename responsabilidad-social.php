<?php
    include 'src/includes/header.php'
?>
    <main>
        <section id="parallax" class="sct-banner scroll">
            <div class="degrade-int"></div>
            <img class="img-banner" src="assets/images/banner/responsabilidad-social.jpg" alt="">
            <div class="content-title-banner container">
                <h1 class="titleBanner text-uppercase">Responsabilidad social</h1>
            </div>
        </section>

        <section class="sct-socialResponsabilitY pt3 w95-movil bg-white">
            <!-- MENSAJEROS DE LA SALUD -->
            <div class="container">
                <div class="row dflex nflex992 pb5">
                    <div class="col-xs-12 col-md-5 wow fadeInRight" data-wow-delay="0.25s">
                        <div class="dta">
                            <div class="dtac">
                                <div class="">
                                    <div class="rs-icon">
                                        <span class="icon-world-persons color-secondary"></span>
                                    </div>
                                    <h2 class="sub-ttl-flotant ttl-presence color-primary">
                                        <p class="text-1">Mensajeros</p>
                                        <p class="text-2">de la salud</p>
                                    </h2>
                                    <p class="text-internas text-justify">Mensajeros de la Salud es un programa que comenzó en el año 1999, integrado por médicos, enfermeras, nutricionistas, psicólogos, y administrativos voluntarios, los cuales llevan campañas médicas gratuitas a diferentes pueblos de bajos recursos que se encuentran alejados y que no pueden acceder a este tipo de chequeos médicos, ya sea por la falta de recursos o porque en su localidad no hay personal médico el cual pueda asistirlo cuando estas personas padecen de algún tipo de enfermedad. </p>
                                    <p class="text-internas text-justify">Esperamos que a este programa se sumen más médicos voluntarios, así como personal voluntario que ayude a contribuir a que este programa se expanda con la finalidad de contribuir con la salud de todos peruanos que más lo necesitan. </p>
                                    <button class="btn-inter">Hazte voluntario</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-7 wow fadeInUp" data-wow-delay="0.75s">
                        <img src="assets/images/internas/pr.jpg" alt="" class="w-100 pl3 mensajeros-salud_img">
                    </div>
                </div>
            </div>
            <!-- MISIONES / CAMPAÑAS / MIL -->
            <div class="misiones-campanas-mil">
                <div class="container">
                    <div class="wrap-mcm">
                        <div class="mcm-item wow fadeInDown" data-wow-delay="0.2s">
                            <h3>104</h3>
                            <h4>misiones</h4>
                            <p>En las zonas más alejadas<br>del sur de nuestro país.</p>
                        </div>
                        <div class="mcm-item wow fadeInDown" data-wow-delay="0.4s">
                            <h3>05</h3>
                            <h4>campañas</h4>
                            <p>Anuales por caso social en<br>atención médica.</p>
                        </div>
                        <div class="mcm-item wow fadeInDown" data-wow-delay="0.6s">
                            <h3>98</h3>
                            <h4>mil</h4>
                            <p>personas atendidas en<br>nuestra clínica hasta el año</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- MENSAJEROS DE LA NOCHE -->
            <div class="container">
                <div class="row dflex flex-reverseMovil992 pb5">
                    <div class="col-xs-12 col-md-7 wow fadeInUp" data-wow-delay="0.25s">
                        <img src="assets/images/internas/rs1.jpg" alt="" class="w-100 pr3 mensajeros-noche_img">
                    </div>
                    <div class="col-xs-12 col-md-5 wow fadeInRight" data-wow-delay="0.75s">
                        <div class="dta">
                            <div class="dtac">
                                <div class="">
                                    <div class="rs-icon">
                                        <span class="icon-cup-bread color-secondary"></span>
                                    </div>
                                    <h2 class="sub-ttl-flotant ttl-presence color-primary">
                                        <p class="text-1">Mensajeros</p>
                                        <p class="text-2">de la noche</p>
                                    </h2>
                                    <p class="text-internas text-justify">Siguiendo el ejemplo y el carisma de San Juan de Dios, la comunidad de hermanos, los colaboradores y el voluntariado de la Clínica, cada quince días salimos en busca de personas que viven en las calles en situaciones vulnerables, brindándoles una bebida caliente y algo de comida, actualmente acompañamos alrededor de 30 personas, sin embargo, el objetivo central es conocer sus necesidades y problemas, para de esta manera ayudarlos, asimismo dar a conocer la palabra de Dios y puedan tener el confort y la tranquilidad en sus vidas.</p>
                                    <p class="text-internas text-justify">Si están interesados en contribuir con esta labor o en acompañarnos a dichas salidas pueden acercarse a nuestra área de Responsabilidad Social.</p>
                                    <button class="btn-inter">Hazte voluntario</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- DONACIONES -->
                <div class="row dflex nflex992 pb5">
                    <div class="col-xs-12 col-md-5 wow fadeInRight" data-wow-delay="0.25s">
                        <div class="dta">
                            <div class="dtac">
                                <div class="">
                                    <div class="rs-icon">
                                        <span class="icon-box-heart color-secondary"></span>
                                    </div>
                                    <h2 class="sub-ttl-flotant ttl-presence color-primary">
                                        <p class="text-2">Donaciones</p>
                                    </h2>
                                    <p class="text-internas text-justify">El área de Responsabilidad Social se encarga de gestionar y administrar eficazmente los recursos obtenidos para brindar atención en salud de niños, jóvenes y adultos de la macro región sur del país en estado de vulnerabilidad, a través de sus diferentes programas sociales; tales como Mensajeros de la Salud, Plan Fundacional, Fundación Teletón, Campaña de Labio Leporino. </p>
                                    <p class="text-internas text-justify">Las donaciones se pueden realizar a través de las cuentas de Clínica si son de dinero, además se perciben donaciones de ropa para niños de 0 a 14 años, ancianos, así como productos de primera necesidad.</p>
                                    <div class="cont-ctas">
                                        <div class="ctas-deposit flex">
                                            <h1 class="numberCta soles">215-12084744-0-08</h1>
                                            <button class="btn-inter">Donaciones</button>
                                        </div>
                                        <div class="ctas-deposit flex">
                                            <h1 class="numberCta dolares">215-12084744-0-08</h1>
                                            <button class="btn-inter">Donaciones</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-7 wow fadeInUp" data-wow-delay="0.75s">
                        <img src="assets/images/internas/rs2.jpg" alt="" class="w-100 pl3 mensajeros-salud_img">
                    </div>
                </div>
            </div>
        </section>
        <?php
            include 'src/includes/footer.php'
        ?>
    </main>
    <?php
            include 'src/includes/cierre.php'
        ?>