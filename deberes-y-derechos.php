<?php
    include 'src/includes/header.php'
?>
<main>
    <section id="parallax" class="sct-banner scroll">
        <div class="degrade-int"></div>
        <img class="img-banner" src="assets/images/banner/informacionAlUsuario.jpg" alt="">
        <div class="content-title-banner container">
            <h1 class="titleBanner text-uppercase">INFORMACIÓN AL USUARIO</h1>
        </div>
    </section>
    <section class="wrapper-info-theClinic bg-white">
        <div class="container-fluid row">
            <div class="description-theClinic col-xs-9 pd-x-0">
                <div class="row">
                    <div class="col-xs-12 col-sm-11 pd-x-0 fr-clinica">
                        <div class="row">
                            <div class="col-xs-12 pd-x-0 paddCustomMovil">
                                <div class="row">
                                    <div class="col-xs-12 pd-x-0">
                                        <h2 class="titles-descrip"><span class="icon-san"></span>
                                            <span class="span-titlesDescrip">
                                                derechos</span><br>y deberes</h2>
                                    </div>
                                    
                                    <!-- DERECHOS DEL PACIENTE -->
                                    <div class="row dflex nflex pb7">
                                        <div class="col-xs-12 col-md-5 wow fadeInRight" data-wow-delay="0.5s">
                                            <div class="dta">
                                                <div class="dtac">
                                                    <div class="">
                                                        <p class="p-internas tabfun3">Derechos del paciente</p>
                                                        <ul>
                                                            <li class="text-internas fleli text-justify">Tener información oportuna y comprensible de su estado de salud.</li>
                                                            <li class="text-internas fleli text-justify">Recibir un trato digno, respetando su privacidad.</li>
                                                            <li class="text-internas fleli text-justify">Ser llamado por su nombre y atendido con amabilidad.</li>
                                                            <li class="text-internas fleli text-justify">Recibir atención de salud de calidad y segura, según protocolos establecidos.</li>
                                                            <li class="text-internas fleli text-justify">Ser informado de los costos de su atención de salud.</li>
                                                            <li class="text-internas fleli text-justify">No ser grabado ni fotografiado con fines de difusión sin su permiso.</li>
                                                            <li class="text-internas fleli text-justify">Que su información médica sea entregada de manera confidencial.</li>
                                                            <li class="text-internas fleli text-justify">Aceptar o rechazar cualquier tratamiento y pedir el alta voluntaria.</li>
                                                            <li class="text-internas fleli text-justify">Recibir visitas, compañía y asistencia espiritual.</li>
                                                            <li class="text-internas fleli text-justify">Consultar o reclamar respecto de la atención de salud recibida.</li>
                                                            <li class="text-internas fleli text-justify">A ser incluido en estudios de investigación científica sólo si lo autoriza.</li>
                                                            <li class="text-internas fleli text-justify">Donde sea pertinente, contar con señalética y facilitadores en lengua originaria.</li>
                                                            <li class="text-internas fleli text-justify">Que el personal de salud porte una identificación.</li>
                                                            <li class="text-internas fleli text-justify">Inscribir el nacimiento de su hijo en el lugar de su residencia.</li>
                                                            <li class="text-internas fleli text-justify">Que su médico le entregue un informe de la atención recibida durante su hospitalización.</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-7 wow fadeInUp" data-wow-delay="0s">
                                            <img src="assets/images/internas/inf-usuario/db1.jpg" alt="" class="w-100 pl3 noPadImg-sm">
                                        </div>
                                    </div>

                                    <!-- DEBERES DEL PACIENTE -->
                                    <div class="row dflex nflex deberes-paciente">
                                        <div class="col-xs-12 col-md-8 wow fadeInUp" data-wow-delay="0s">
                                            <img src="assets/images/internas/inf-usuario/db2.jpg" alt="" class="w-100 pr3 noPadImg-sm">
                                        </div>
                                        <div class="col-xs-12 col-md-4 wow fadeInRight" data-wow-delay="0.25s">
                                            <div class="dta">
                                                <div class="dtac">
                                                    <p class="p-internas tabfun3">Deberes<br>del paciente</p>
                                                    <ul>
                                                        <li class="text-internas fleli text-justify">Entregar información veraz acerca de su enfermedad, identidad y dirección.</li>
                                                        <li class="text-internas fleli text-justify">Conocer y cumplir el reglamento interno y resguardar su información médica.</li>
                                                        <li class="text-internas fleli text-justify">Cuidar las instalaciones y equipamiento del recinto.</li>
                                                        <li class="text-internas fleli text-justify">Informarse acerca de los horarios de atención y formas de pago.</li>
                                                        <li class="text-internas fleli text-justify">Tratar respetuosamente al personal de salud.</li>
                                                        <li class="text-internas fleli text-justify">Informarse acerca de los procedimientos de reclamo.</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
            <!--NAVBAR THE OH-->
            <?php
                include 'src/includes/navbar-inf-usuario.php'
            ?>
        </div>
    </section>
</main>
<?php
    include 'src/includes/footer.php'
?>

</body>

</html>