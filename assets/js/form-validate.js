$('#btn-send-form').on('click', function(){
    $("#form-contact").validate({
        rules: {
            "name": "required",
            "lastname": "required",
            "phone": {
                required: true,
                minlength: 9
            },
            "email": {
                required: true,
                email: true
            },
            "textarea": {
                required: true
            }
            
        },
    
    });
});

//FORM TRABAJA CON NOSOTROS 
$('#btn-form-postulante').on('click', function(){
    $("#form-post").validate({
        rules: {
            "name-postulante": "required",
            "centro": "required",
            "profesion": "required",
            "pais": "required",
            "lastname-postulante": "required",
            "phone-postulante": {
                required: true,
                minlength: 9
            },
        },
    });
})